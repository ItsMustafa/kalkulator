def operasi():
    pilihan = input('''
Mohon masukkan kode yang anda pakai untuk perhitungan:
1 untuk penjumlahan(+)
2 untuk pengurangan(-)
3 untuk perkalian(*)
4 untuk pembagian(/)
''')

    bil1 = int(input('Silahkan masukkan bilangan pertama: '))
    bil2 = int(input('Silahkan masukkan bilangan kedua: '))

    if pilihan == '1':
        print('{} + {} = '.format(bil1, bil2))
        print("Hasilnya",bil1 + bil2)

    elif pilihan == '2':
        print('{} - {} = '.format(bil1, bil2))
        print("Hasilnya",bil1 - bil2)

    elif pilihan == '3':
        print('{} * {} = '.format(bil1, bil2))
        print("Hasilnya",bil1 * bil2)

    elif pilihan == '4':
        print('{} / {} = '.format(bil1, bil2))
        print("Hasilnya",bil1 / bil2)

    else:
        print('Salah inputan.')

    lagi()

def lagi():
    hitung_lagi = input('''
Mau berhitung lagi?
Mohon ketik Y untuk YA atau T untuk TIDAK.
''')

    if hitung_lagi.upper() == 'Y':
        operasi()
    elif hitung_lagi.upper() == 'T':
        print('Jumpa lagi.')
    else:
        lagi()

operasi()